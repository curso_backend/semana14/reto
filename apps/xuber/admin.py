from django.contrib import admin
from .models import Usuario, UsuarioAdmin, Rol, Vehiculo, Lugar, Destino_favorito, Viaje


# Register your models here.
admin.site.register(Usuario, UsuarioAdmin)
admin.site.register(Rol)
admin.site.register(Vehiculo)
admin.site.register(Lugar)
admin.site.register(Destino_favorito)
admin.site.register(Viaje)
