from django.db import models
from django.contrib.auth.models import User
from django.contrib import admin

class Rol(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100, blank=False, null=False)

    class Meta:
        verbose_name = 'Rol'
        verbose_name_plural = 'Roles'
        ordering = ['nombre']

    def __str__(self):
        return self.nombre

class Vehiculo(models.Model):
    id = models.AutoField(primary_key=True)
    modelo = models.CharField(max_length=150)
    placa = models.CharField(max_length=150)

    class Meta:
        verbose_name = 'Vehículo'
        verbose_name_plural = 'Vehículos'
        ordering = ['modelo']

    def __str__(self):
        return self.modelo

class Usuario(models.Model):
    id = models.AutoField(primary_key=True)
    nombres = models.CharField(max_length=150)
    apellidos = models.CharField(max_length=150)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    rol = models.ManyToManyField(Rol, through='RolUsuario', through_fields=('usuario_id', 'rol_id'))
    vehiculo = models.ManyToManyField(Vehiculo, through='ConductorVehiculo', through_fields=('usuario_id', 'vehiculo_id'))

    class Meta:
        verbose_name = 'Usuario'
        verbose_name_plural = 'Usuarios'
        ordering = ['apellidos']

    def __str__(self):
        return f'{self.apellidos} {self.nombres}'

class RolUsuario(models.Model):
    id = models.AutoField(primary_key=True)
    usuario_id = models.ForeignKey(Usuario, on_delete=models.CASCADE,)
    rol_id = models.ForeignKey(Rol, on_delete=models.CASCADE,)

    class Meta:
        verbose_name = 'Rol Usuario'
        verbose_name_plural = 'Roles Usuario'

    def __str__(self):
        return f'{self.usuario_id} / {self.rol_id}'

class ConductorVehiculo(models.Model):
    id = models.AutoField(primary_key=True)
    usuario_id = models.ForeignKey(Usuario, on_delete=models.CASCADE,)
    vehiculo_id = models.ForeignKey(Vehiculo, on_delete=models.CASCADE,)
    estados_conductor_vehiculo = ((1, "Activo"), (2, "Inactivo"))
    estado = models.IntegerField(choices=estados_conductor_vehiculo, default=1, null=False, blank=False,)

    class Meta:
        verbose_name = 'Conductor Vehiculo'
        verbose_name_plural = 'Conductor Vehículos'

    def __str__(self):
        return f'{self.usuario_id} / {self.vehiculo_id}'

class RolUsuario_inline(admin.TabularInline):
    model = RolUsuario
    extra = 1

class ConductorVehiculo_inline(admin.TabularInline):
    model = ConductorVehiculo
    extra = 1

class UsuarioAdmin(admin.ModelAdmin):
    inlines = (RolUsuario_inline, ConductorVehiculo_inline)

class Lugar(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=150)

    class Meta:
        verbose_name = 'Lugar'
        verbose_name_plural = 'Lugares'

    def __str__(self):
        return f'{self.nombre}'

class Destino_favorito(models.Model):
    id = models.AutoField(primary_key=True)
    cliente = models.OneToOneField(User, on_delete=models.CASCADE, related_name='cliente_destino_favorito')
    lugar = models.ForeignKey(Lugar, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Destino Favorito'
        verbose_name_plural = 'Destinos Favoritos'

    def __str__(self):
        return f'{self.lugar}'

class Viaje(models.Model):
    conductor = models.OneToOneField(User, on_delete=models.CASCADE, related_name='conductor')
    cliente = models.OneToOneField(User, on_delete=models.CASCADE, related_name='cliente')
    origen = models.OneToOneField(Lugar, on_delete=models.CASCADE, related_name='origen')
    destino = models.OneToOneField(Destino_favorito, on_delete=models.CASCADE, related_name='destino')
    tarifa = models.FloatField(null=True, blank=True,)
    puntajes = ((1, "Pésimo"), (2, "Malo"), (3, "Regular"), (4, "Bueno"), (5, "Excelente"))
    puntaje_conductor = models.IntegerField(choices=puntajes, default=5)
    puntaje_cliente = models.IntegerField(choices=puntajes, default=5)
    estados_viaje = ((1, "Solicitar"), (2, "En ruta"), (3, "Finalizado"))
    estado = models.IntegerField(choices=estados_viaje, default=1, null=False, blank=False,)

    class Meta:
        verbose_name = 'Viaje'
        verbose_name_plural = 'Viajes'

    def __str__(self):
        return f'{self.origen} / {self.destino}'
