from django import forms
from .models import Rol, Vehiculo, Usuario, RolUsuario, ConductorVehiculo, ConductorVehiculo
from .models import Lugar, Destino_favorito, Viaje

class RolForm(forms.ModelForm):
    class Meta:
        model = Rol
        fields = ['nombre']
        labels = {
            'nombre': 'Tipo'
        }
        widgets = {
            'nombre': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingresa el tipo de usuario (conductor o cliente)',
                    'id': 'nombre'
                }
            )
        }

class VehiculoForm(forms.ModelForm):
    class Meta:
        model = Vehiculo
        fields = ['modelo','placa']
        labels = {
            'modelo': 'Modelo y marca del vehículo',
            'placa': 'Placa del vehículo'
        }
        widgets = {
            'modelo' : forms.TextInput(attrs={'class':'form-control'}),
            'placa' : forms.TextInput(attrs={'class':'form-control'})
        }

class UsuarioForm(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = ['nombres', 'apellidos', 'user', 'rol', 'vehiculo']
        labels = {
            'nombres': 'Nombres',
            'apellidos': 'Apellidos',
            'user': 'Usuario',
            'rol': 'Tipo de usuario',
            'vehiculo': 'Vehículo'
        }
        widgets = {
            'nombres' : forms.TextInput(attrs={'class':'form-control'}),
            'apellidos' : forms.TextInput(attrs={'class':'form-control'}),
            'user' : forms.Select(attrs={'class':'form-control'}),
            'rol' : forms.Select(attrs={'class':'form-control'}),
            'vehiculo' : forms.Select(attrs={'class':'form-control'})
        }

class RolUsuarioForm(forms.ModelForm):
    class Meta:
        model = RolUsuario
        fields = ['usuario_id', 'rol_id']
        labels = {
            'usuario_id': 'Usuario',
            'rol_id': 'Tipo de Usuario'
        }
        widgets = {
            'usuario_id' : forms.Select(attrs={'class':'form-control'}),
            'rol_id' : forms.Select(attrs={'class':'form-control'})
        }

class ConductorVehiculoForm(forms.ModelForm):
    class Meta:
        model = ConductorVehiculo
        fields = ['usuario_id', 'vehiculo_id', 'estado']
        labels = {
            'usuario_id': 'Usuario',
            'vehiculo_id': 'Vehiculo',
            'estado': 'Actvidad de vehículo (1.Activo ó 2.Inactivo)'
        }
        widgets = {
            'usuario_id' : forms.Select(attrs={'class':'form-control'}),
            'vehiculo_id' : forms.Select(attrs={'class':'form-control'}),
            'estado' : forms.Select(attrs={'class':'form-control'})
        }

class LugarForm(forms.ModelForm):
    class Meta:
        model = Lugar
        fields = ['nombre']
        labels = {
            'nombre': 'Dirección'
        }
        widgets = {
            'nombre' : forms.TextInput(attrs={'class':'form-control'})
        }
    
class Destino_favoritoForm(forms.ModelForm):
    class Meta:
        model = Destino_favorito
        fields = ['cliente', 'lugar']
        labels = {
            'cliente': 'Cliente',
            'lugar': 'Dirección'
        }  
        widgets = {
            'cliente' : forms.Select(attrs={'class':'form-control'}),
            'lugar' : forms.Select(attrs={'class':'form-control'})
        }